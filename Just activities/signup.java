package com.example.ass_1;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class signup extends AppCompatActivity {

    private static String fullname;

    public static String getValue() {
        return fullname;
    }

    private static String pass;

    public static String getValue1() {
        return pass;
    }

    private static String email;

    public static String getValue2() {
        return email;
    }

    private static String phone;

    public static String getValue3() {
        return phone;
    }

    private static String address;

    public static String getValue4() {
        return address;
    }

    private static String roll;

    public static String getValue5() {
        return roll;
    }


    ImageView mimageview;
    Button mChoosebtn;

    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;


    private EditText edUsername;
    private EditText edPassword;
    private EditText edConfirmPassword;
    private Button btnCreateUser;
    private EditText edphone;
    private EditText edAddress;
    private EditText edfname;
    private EditText edlname;
    private EditText ed_roll_no1;

    private final String CREDENTIAL_SHARED_PREF = "our_shared_pref";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);


        mimageview = findViewById(R.id.image_view);
        mChoosebtn = findViewById(R.id.choose_image_btn);


        mChoosebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
//permission not granted , request it;

                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        //show popup for runtime permission;
                        requestPermissions(permissions, PERMISSION_CODE);
                    } else {
                        //permission already granted
                        pickImageFromGallery();
                    }
                } else {
                    pickImageFromGallery();
                    //system is less than 5.0
                }
            }
        });


        edUsername = findViewById(R.id.ed_username);
        edPassword = findViewById(R.id.ed_password);
        edConfirmPassword = findViewById(R.id.ed_confirm_pass);
        btnCreateUser = findViewById(R.id.btn_create_user);
        edphone = findViewById(R.id.ed_phone);
        edAddress = findViewById(R.id.ed_Address);
        edfname = findViewById(R.id.fname);
        edlname = findViewById(R.id.lname);
        ed_roll_no1 = findViewById(R.id.ed_roll_no);


        btnCreateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strPassword = edPassword.getText().toString();
                String strUsername = edUsername.getText().toString();
                String strConfirmPassword = edConfirmPassword.getText().toString();
                String strphone = edphone.getText().toString();
                String strAddress = edAddress.getText().toString();
                String strfname = edfname.getText().toString();
                String strlname = edlname.getText().toString();
                String strroll = ed_roll_no1.getText().toString();


                if (strPassword != null && strConfirmPassword != null && strPassword.equalsIgnoreCase(strConfirmPassword)) {

                    if (strPassword.equalsIgnoreCase("")) {
                        if (strUsername.equalsIgnoreCase("")) {
                            edUsername.setError("Email is must is must");
                            if (strConfirmPassword.equalsIgnoreCase("")) {
                                edConfirmPassword.setError("please confirm password");
                                if (strphone.equalsIgnoreCase("")) {
                                    edphone.setError("Enter phone number");
                                    if (strfname.equalsIgnoreCase("")) {
                                        edfname.setError("Enter first name");
                                        if (strlname.equalsIgnoreCase("")) {
                                            edlname.setError("Enter last name");
                                            if (strroll.equalsIgnoreCase("")) {
                                                /*char[] ch = new char[strroll.length()];


                                                for (int i = 0; i < strroll.length(); i++) {
                                                    ch[i] = strroll.charAt(i);
                                                }
                                                if (ch[2] != 'F' && ch[2] != 'f') {
                                                    ed_roll_no1.setError("your format is wrong");

                                                }
                                                ed_roll_no1.setError("your format is wrong");*/

                                            }
                                        }
                                    }
                                }
                            }

                        }

                        edPassword.setError("Password is must");
                    } else {
                        SharedPreferences credentials = getSharedPreferences(CREDENTIAL_SHARED_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = credentials.edit();
                        editor.putString("Password", strPassword);
                        editor.putString("Username", strUsername);
                        editor.commit();
                        String str_full_name = strfname + " " + strlname;

                        fullname = str_full_name;
                        pass = strPassword;
                        email = strUsername;
                        phone = strphone;
                        address = strAddress;
                        roll = strroll;

                        Intent intent = new Intent(signup.this, MainActivity.class);
                        startActivity(intent);

                        signup.this.finish();
                    }
                }


            }
        });

    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/+");
        startActivityForResult(intent, IMAGE_PICK_CODE);


        //handle result of runtime permision

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission granted
                    pickImageFromGallery();
                } else {
                    //permission denied
                    Toast.makeText(this, "Permission denied...", Toast.LENGTH_SHORT).show();

                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE) {

            mimageview.setImageURI(data.getData());

        }


    }
}