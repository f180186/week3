package com.example.ass_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

public class MainActivity extends AppCompatActivity {

    String str = signup.getValue();
    String str1 = signup.getValue1();
    String str2 = signup.getValue2();
    String str3 = signup.getValue3();
    String str4 = signup.getValue4();
    String str6 = signup.getValue5();


    private EditText edUsername;
    private EditText edPassword;
    private MaterialButton btnLogin;
    private MaterialButton btnSignup;

    private static String fullname;

    public static String getValue() {
        return fullname;
    }

    private static String pass;

    public static String getValue1() {
        return pass;
    }

    private static String email;

    public static String getValue2() {
        return email;
    }

    private static String phone;

    public static String getValue3() {
        return phone;
    }

    private static String address;

    public static String getValue4() {
        return address;
    }

    private static String roll;

    public static String getValue5() {
        return roll;
    }


    private final String CREDENTIAL_SHARED_PREF = "our_shared_pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView username = (TextView) findViewById(R.id.username);
        TextView password = (TextView) findViewById(R.id.password);
        TextView frgtpass = (TextView) findViewById(R.id.frgthide);

        MaterialButton loginbtn = (MaterialButton) findViewById(R.id.loginbtn);
        MaterialButton frgt = (MaterialButton) findViewById(R.id.frgt);
        MaterialButton frgtbtn = (MaterialButton) findViewById(R.id.frgtbtn);
        MaterialButton btn_signup = (MaterialButton) findViewById(R.id.btn_signup);

        ImageView img = (ImageView) findViewById(R.id.fb);
        ImageView img1 = (ImageView) findViewById(R.id.twitter);
        ImageView img2 = (ImageView) findViewById(R.id.insta);

        edUsername = findViewById(R.id.username);
        edPassword = findViewById(R.id.password);
        btnLogin = findViewById(R.id.loginbtn);
        btnSignup = findViewById(R.id.btn_signup);


        //admin and admin

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, signup.class);
                startActivity(intent);

            }
        });
        /*Bundle bundle = getIntent().getExtras();
        String email=bundle.getString("email1");
        String full_name=bundle.getString("fullname1");
        String password1=bundle.getString("pass1");
        String phone=bundle.getString("phone1");
        String address=bundle.getString("address1");*/


        btnLogin.setOnClickListener(new View.OnClickListener() {


            /*Bundle bundle=getIntent().getExtras();

            String fullname=bundle.getString("full_name");
            String email=bundle.getString("email");
            String password1=bundle.getString("password");
            String phone=bundle.getString("phone");
            String address=bundle.getString("address");*/


            @Override
            public void onClick(View view) {


                /*if(username.getText().toString().equals("f180186") && password.getText().toString().equals("12345"))
                {
                    //correct
                    Toast.makeText(MainActivity.this,"Login succeasfull",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(MainActivity.this,"Not succesfull",Toast.LENGTH_SHORT).show();
                    //incorrect
                }*/

                SharedPreferences credentials = getSharedPreferences(CREDENTIAL_SHARED_PREF, Context.MODE_PRIVATE);
                String strUsername = credentials.getString("Username", null);
                String strPassword = credentials.getString("Password", null);

                String username_from_ed = edUsername.getText().toString();

                String password_from_ed = edPassword.getText().toString();
                if (strUsername != null && username_from_ed != null && strUsername.equalsIgnoreCase(username_from_ed)) {
                    if (strPassword != null && password_from_ed != null && strPassword.equalsIgnoreCase(password_from_ed)) {
                        Toast.makeText(MainActivity.this, "Login succeasfull", Toast.LENGTH_SHORT).show();

                       /* Bundle bundle= new Bundle();
                        bundle.putString("fullname1",full_name);
                        bundle.putString("email1",email);
                        bundle.putString("pass1",password1);
                        bundle.putString("phone1",phone);
                        bundle.putString("address1",address);*/
                        fullname = str;
                        email = str2;
                        pass = str1;
                        phone = str3;
                        address = str4;
                        roll = str6;
                        Intent intent = new Intent(MainActivity.this, homescreen.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "Login UnSucceasfull", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        frgt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                frgtpass.setVisibility(View.VISIBLE);
                frgt.setVisibility(View.INVISIBLE);
                frgtbtn.setVisibility((View.VISIBLE));
                btn_signup.setVisibility(View.INVISIBLE);

            }
        });
        frgtbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (frgtpass.getText().toString().equals("21")) {
                    //correct
                    Toast.makeText(MainActivity.this, "Okay Verified, Your password is 12345", Toast.LENGTH_SHORT).show();
                    frgtpass.setVisibility(View.INVISIBLE);
                    frgtbtn.setVisibility(view.INVISIBLE);
                    btn_signup.setVisibility(view.VISIBLE);
                } else {
                    Toast.makeText(MainActivity.this, "Sorry", Toast.LENGTH_SHORT).show();
                    frgtpass.setVisibility(View.INVISIBLE);
                    frgtbtn.setVisibility(view.INVISIBLE);
                    btn_signup.setVisibility(view.VISIBLE);

                    //incorrect
                }
            }
        });


    }


}