package com.example.ass_1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class homescreen extends AppCompatActivity {

    ImageView mimageview;


    @Override


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);

        TextView edfullname = (TextView) findViewById(R.id.full_name);
        TextView edName = (TextView) findViewById(R.id.namefield);
        TextView edpass = (TextView) findViewById(R.id.passfield);
        TextView edphone = (TextView) findViewById(R.id.phonefield);
        TextView edaddress = (TextView) findViewById(R.id.addressfield);
        TextView payment = (TextView) findViewById(R.id.payment_label);
        Button btn1 = (Button) findViewById(R.id.exitbtn);


        edfullname.setText(MainActivity.getValue());
        edpass.setText(MainActivity.getValue1());
        edName.setText(MainActivity.getValue());
        edphone.setText(MainActivity.getValue3());
        edaddress.setText(MainActivity.getValue4());
        payment.setText(MainActivity.getValue5());
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackOressed();

            }
        });


    }

    public void onBackOressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(homescreen.this);
        builder.setMessage("Are you sure want to exit ?");
        builder.setCancelable(true);
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setPositiveButton("Close!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        AlertDialog a = builder.create();
        a.show();

    }


}